package com.example.crudsqlite.model;

/**
 * Dibuat oleh petersam
 */
public class Product {
    private int id, quantity;
    private String name;

    public Product(int quantity, String name) {
        this.quantity = quantity;
        this.name = name;
    }

    public Product(int id, int quantity, String name) {
        this.id = id;
        this.quantity = quantity;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}